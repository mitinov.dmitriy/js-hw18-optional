if (typeof window !== "undefined") {
    window.addEventListener("load", function () {

        function cloneObject(obj) {
            const clonedObject = {};
            for(const Key in obj) {
                if(typeof(obj[Key])==="object" && obj[Key] !== null)
                    clonedObject[Key] = cloneObject(obj[Key]);
                else
                    clonedObject[Key] = obj[Key];
            }
            return clonedObject;
        }

    });
}
